import React, {useEffect, useState} from 'react';
import * as eva from '@eva-design/eva';
import {default as themeLight} from './src/themes/light-theme.json';

import {
  ApplicationProvider,
  IconRegistry,
  Icon,
  Layout,
} from '@ui-kitten/components';
import {
  SafeAreaView,
  Image,
  StyleSheet,
  StatusBar,
  View,
  TouchableOpacity,
} from 'react-native';
import {EvaIconsPack} from '@ui-kitten/eva-icons';
import LogoString from './src/assets/LogoString';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import TabController from './src/app';
import Profile from './src/app/routes/Profile';
import Search from './src/app/routes/Search';
import {connect} from 'react-redux';

const SearchIcon = props => <Icon {...props} name="search-outline" />;

const Stack = createStackNavigator();
const App = props => {
  const size = 40;
  const AppHome = ({navigation, app_settings}) => {
    const {theme} = app_settings;
    return (
      <>
        <SafeAreaView>
          <View
            style={{
              ...styles.container,
              backgroundColor: theme['header-color'],
            }}>
            <LogoString />
            <View
              style={{
                ...styles.left,
                backgroundColor: theme['header-color'],
              }}>
              <TouchableOpacity
                onPress={() => navigation.navigate('Search')}
                hitSlop={{
                  top: 2,
                  left: 2,
                  bottom: 2,
                  right: 2,
                }}>
                <SearchIcon
                  style={{...styles.subItem, width: 25, height: 25}}
                  fill={theme['color-basic-600']}
                />
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => navigation.navigate('Profile')}
                underlayColor={theme['color-basic-700']}>
                <Image
                  source={{
                    uri:
                      'https://image.flaticon.com/icons/png/512/168/168728.png',
                  }}
                  style={{
                    width: size,
                    height: size,
                    ...styles.subItem,
                    borderRadius: size / 2,
                  }}
                />
              </TouchableOpacity>
            </View>
          </View>
        </SafeAreaView>
        <TabController />
      </>
    );
  };
  const mapStateToProps = state => {
    const {app_settings} = state;
    return {
      app_settings,
    };
  };
  const AppHomeConnected = connect(mapStateToProps)(AppHome);
  return (
    <ApplicationProvider {...eva} theme={props.app_settings.theme}>
      <StatusBar
        barStyle={props.byDefault ? 'dark-content' : 'light-content'}
        hidden={false}
        backgroundColor={props.app_settings.theme['color-basic-200']}
      />
      <IconRegistry icons={EvaIconsPack} />
      <NavigationContainer>
        <Stack.Navigator
          mode="modal"
          headerMode="none"
          screenOptions={{
            cardStyle: {
              backgroundColor: props.app_settings.theme['header-color'],
            },
          }}>
          <Stack.Screen
            options={{headerShown: false}}
            name="AppHome"
            component={AppHomeConnected}
          />
          <Stack.Screen name="Search" component={Search} />
          <Stack.Screen name="Profile" component={Profile} />
        </Stack.Navigator>
      </NavigationContainer>
    </ApplicationProvider>
  );
};

const styles = StyleSheet.create({
  container: {
    marginHorizontal: 20,
    marginVertical: 10,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  left: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
    width: 100,
  },
  subItem: {
    marginLeft: 0,
  },
});

const mapStateToProps = state => {
  const {app_settings} = state;
  return {
    app_settings,
  };
};
export default connect(mapStateToProps)(App);

// del %appdata%\Temp\react-* & del %appdata%\Temp\metro-* & del %appdata%\Temp\haste-* & del node_modules & npm install & watchman watch-del-all & npm cache clean --force & react-native run-android & react-native start
