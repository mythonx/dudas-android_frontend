import React from 'react';
import * as eva from '@eva-design/eva';
import {default as themeDark} from '../themes/dark-theme.json';
import {default as themeLight} from '../themes/light-theme.json';
import {APP_SETTINGS} from './types';

export const updateAppSettings = app_settings => {
  app_settings['theme'] = app_settings['byDefault']
    ? {...eva.light, ...themeLight}
    : {
        ...eva.dark,
        ...themeDark,
      };
  return {
    type: APP_SETTINGS,
    payload: app_settings,
  };
};
