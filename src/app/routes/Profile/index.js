import {View, StyleSheet, Image, TouchableOpacity} from 'react-native';
import {Text, Button, Icon, Layout} from '@ui-kitten/components';
import React, {useEffect, useState} from 'react';
import {bindActionCreators} from 'redux';
import {updateAppSettings} from '../../../actions';
import {connect} from 'react-redux';
import LogoString from '../../../assets/LogoString';
import LoadingIndicator from '../../../components/Loader';

const Profile = ({navigation, app_settings, updateAppSettings}) => {
  const colorChanged = true;
  const size = 300;
  const FileIcon = props => <Icon {...props} name="file-add-outline" />;
  const ModeIcon = props => (
    <Icon
      {...props}
      name={app_settings.byDefault ? 'moon-outline' : 'sun-outline'}
    />
  );
  return (
    <>
      <Layout style={styles.header}>
        <LogoString />
        <Button
          onPress={() => {
            updateAppSettings({
              byDefault: !app_settings.byDefault,
              theme: null,
            });
          }}
          style={styles.button}
          appearance="outline"
          accessoryRight={ModeIcon}
        />
      </Layout>

      <Layout style={styles.container}>
        <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
          <TouchableOpacity onPress={() => navigation.navigate('Profile')}>
            <>
              <FileIcon style={{width: 50, height: 50}} />
              <Image
                source={{
                  uri:
                    'https://image.flaticon.com/icons/png/512/168/168728.png',
                }}
                style={{
                  width: size,
                  height: size,
                  ...styles.subItem,
                  borderRadius: size / 2,
                }}
              />
            </>
          </TouchableOpacity>
          <Text style={{fontSize: 30}}>This is a modal!</Text>
        </View>
      </Layout>
    </>
  );
};
const styles = StyleSheet.create({
  container: {
    flexDirection: 'column',
    height: '90%',
    paddingVertical: '10%',
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  header: {
    flexDirection: 'row',
    height: '10%',
    paddingHorizontal: '5%',
    justifyContent: 'space-between',
    alignItems: 'center',
    width: '100%',
  },
  button: {
    margin: 2,
  },
});
const mapStateToProps = state => {
  const {app_settings} = state;
  return {
    app_settings,
  };
};
const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      updateAppSettings: updateAppSettings,
    },
    dispatch,
  );
};
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Profile);
