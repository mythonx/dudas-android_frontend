import Home from './Home';
import Settings from './Settings';
import Messages from './Messages';
import Community from './Community';
import Following from './Following';

export default {
  Home,
  Settings,
  Messages,
  Community,
  Following,
};
