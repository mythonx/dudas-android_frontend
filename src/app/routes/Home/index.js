import React, {useState} from 'react';
import {View, SafeAreaView, StyleSheet, ScrollView} from 'react-native';
import {
  TopNavigation,
  Layout,
  Button,
  Card,
  Text,
  Divider,
  Icon,
  Input,
} from '@ui-kitten/components';
import {connect} from 'react-redux';
import {createStackNavigator} from '@react-navigation/stack';
import Wall from './Wall';
import {CreateSomething} from './CreateSomething';

const Stack = createStackNavigator();
const Home = ({navigation, app_settings}) => {
  const {theme} = app_settings;
  return (
    <SafeAreaView
      style={{
        flex: 1,
        backgroundColor: theme['header-color'],
      }}>
      <Stack.Navigator>
        <Stack.Screen
          name="Wall"
          options={{headerShown: false}}
          component={Wall}
          initialParams={{
            styles: {
              height: '100%',
              backgroundColor: theme['color-basic-1000'],
            },
          }}
        />
        <Stack.Screen
          name="CreateSomething"
          options={{headerShown: false}}
          component={CreateSomething}
        />
      </Stack.Navigator>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  topContainer: {
    flexDirection: 'row',
  },
  card: {
    flex: 1,
    margin: 2,
  },
  footerContainer: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
  },
  footerControl: {
    // marginHorizontal: 2,
  },
});
const mapStateToProps = state => {
  const {app_settings} = state;
  return {
    app_settings,
  };
};
export default connect(mapStateToProps)(Home);
