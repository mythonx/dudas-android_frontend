import {
  Button,
  Card,
  Divider,
  Select,
  Icon,
  Layout,
  SelectItem,
  Text,
  IndexPath,
} from '@ui-kitten/components';
import {ScrollView, StyleSheet} from 'react-native';
import React, {useState} from 'react';
import {bindActionCreators} from 'redux';
import {updateAppSettings} from '../../../actions';
import {connect} from 'react-redux';

const Wall = ({navigation, route, app_settings}) => {
  const {theme} = app_settings;
  const {styles} = route.params;
  const [selectedIndex, setSelectedIndex] = useState(new IndexPath(0));
  const [wasChanged, setWasChanged] = useState(false);
  const data = [
    {label: 'Preguntar', key: 'ask', icon: 'question-mark-circle-outline'},
    {
      label: 'Publicar un tema',
      key: 'post',
      icon: 'message-square-outline',
    },
    {
      label: 'Proponer una reunion',
      key: 'meetup',
      icon: 'tv-outline',
    },
  ];
  const displayValue = data[selectedIndex.row].label;
  const LeftIcon = props => (
    <Icon {...props} name={data[selectedIndex.row].icon} />
  );
  const ArrowIcon = props => (
    <Icon {...props} name="arrow-ios-downward-outline" />
  );
  const EditIcon = props => <Icon {...props} name="edit-2-outline" />;
  let w = wasChanged ? {width: '80%'} : {width: '100%'};
  return (
    <Layout
      style={{
        ...styles,
        paddingVertical: 15,
        paddingHorizontal: 10,
      }}>
      <Layout
        style={{
          flexDirection: 'row',
          justifyContent: 'center',
          marginBottom: 10,
        }}>
        <Select
          style={{...styles.input, ...w}}
          value={wasChanged ? displayValue : 'Párticipar'}
          placeholder="Párticipar"
          selectedIndex={selectedIndex}
          accessoryLeft={wasChanged ? LeftIcon : EditIcon}
          accessoryRight={ArrowIcon}
          size={'large'}
          onSelect={index => {
            setWasChanged(true);
            setSelectedIndex(index);
          }}>
          {data.map(action => (
            <SelectItem
              key={action.key}
              title={action.label}
              accessoryLeft={props => <Icon {...props} name={action.icon} />}
            />
          ))}
        </Select>
        {wasChanged && (
          <Button
            style={{width: '20%'}}
            onPress={() => navigation.navigate('CreateSomething')}
            appearance="outline"
            accessoryLeft={EditIcon}
          />
        )}
      </Layout>
      <Divider />
      <ScrollView showsVerticalScrollIndicator={false}>
        <Card style={{padding: 5, marginBottom: 5}}>
          <Text>
            The Maldives, officially the Republic of Maldives, is a small
            country in South Asia, located in the Arabian Sea of the Indian
            Ocean. It lies southwest of Sri Lanka and India, about 1,000
            kilometres (620 mi) from the Asian continent
          </Text>
        </Card>
        <Card style={{padding: 5, marginBottom: 5}}>
          <Text>
            The Maldives, officially the Republic of Maldives, is a small
            country in South Asia, located in the Arabian Sea of the Indian
            Ocean. It lies southwest of Sri Lanka and India, about 1,000
            kilometres (620 mi) from the Asian continent
          </Text>
        </Card>
        <Card style={{padding: 5, marginBottom: 5}}>
          <Text>
            The Maldives, officially the Republic of Maldives, is a small
            country in South Asia, located in the Arabian Sea of the Indian
            Ocean. It lies southwest of Sri Lanka and India, about 1,000
            kilometres (620 mi) from the Asian continent
          </Text>
        </Card>
        <Card style={{padding: 5, marginBottom: 5}}>
          <Text>
            The Maldives, officially the Republic of Maldives, is a small
            country in South Asia, located in the Arabian Sea of the Indian
            Ocean. It lies southwest of Sri Lanka and India, about 1,000
            kilometres (620 mi) from the Asian continent
          </Text>
        </Card>
        <Card style={{padding: 5, marginBottom: 5}}>
          <Text>
            The Maldives, officially the Republic of Maldives, is a small
            country in South Asia, located in the Arabian Sea of the Indian
            Ocean. It lies southwest of Sri Lanka and India, about 1,000
            kilometres (620 mi) from the Asian continent
          </Text>
        </Card>
        <Card style={{padding: 5, marginBottom: 5}}>
          <Text>
            The Maldives, officially the Republic of Maldives, is a small
            country in South Asia, located in the Arabian Sea of the Indian
            Ocean. It lies southwest of Sri Lanka and India, about 1,000
            kilometres (620 mi) from the Asian continent
          </Text>
        </Card>
        <Card style={{padding: 5, marginBottom: 5}}>
          <Text>
            The Maldives, officially the Republic of Maldives, is a small
            country in South Asia, located in the Arabian Sea of the Indian
            Ocean. It lies southwest of Sri Lanka and India, about 1,000
            kilometres (620 mi) from the Asian continent
          </Text>
        </Card>
        <Card style={{padding: 5, marginBottom: 5}}>
          <Text>
            The Maldives, officially the Republic of Maldives, is a small
            country in South Asia, located in the Arabian Sea of the Indian
            Ocean. It lies southwest of Sri Lanka and India, about 1,000
            kilometres (620 mi) from the Asian continent
          </Text>
        </Card>
        <Card style={{padding: 5, marginBottom: 5}}>
          <Text>
            The Maldives, officially the Republic of Maldives, is a small
            country in South Asia, located in the Arabian Sea of the Indian
            Ocean. It lies southwest of Sri Lanka and India, about 1,000
            kilometres (620 mi) from the Asian continent
          </Text>
        </Card>
        <Card style={{padding: 5, marginBottom: 5}}>
          <Text>
            The Maldives, officially the Republic of Maldives, is a small
            country in South Asia, located in the Arabian Sea of the Indian
            Ocean. It lies southwest of Sri Lanka and India, about 1,000
            kilometres (620 mi) from the Asian continent
          </Text>
        </Card>
        <Card style={{padding: 5, marginBottom: 5}}>
          <Text>
            The Maldives, officially the Republic of Maldives, is a small
            country in South Asia, located in the Arabian Sea of the Indian
            Ocean. It lies southwest of Sri Lanka and India, about 1,000
            kilometres (620 mi) from the Asian continent
          </Text>
        </Card>
        <Card style={{padding: 5, marginBottom: 5}}>
          <Text>
            The Maldives, officially the Republic of Maldives, is a small
            country in South Asia, located in the Arabian Sea of the Indian
            Ocean. It lies southwest of Sri Lanka and India, about 1,000
            kilometres (620 mi) from the Asian continent
          </Text>
        </Card>
      </ScrollView>
    </Layout>
  );
};
const styles = StyleSheet.create({
  input: {
    flex: 1,
    margin: 2,
  },
});

const mapStateToProps = state => {
  const {app_settings} = state;
  return {
    app_settings,
  };
};
export default connect(mapStateToProps)(Wall);
