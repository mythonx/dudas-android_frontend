import React, {useState} from 'react';
import {StyleSheet, TextInput, Text} from 'react-native';
import {Button, Icon, Layout} from '@ui-kitten/components';
const useInputState = (initialValue = '') => {
  const [value, setValue] = useState(initialValue);
  return {value, onChangeText: setValue};
};

const Post = ({navigation}) => {
  const smallInputState = useInputState();
  const mediumInputState = useInputState();
  const title = useInputState();
  const description = useInputState();

  const PublishIcon = props => <Icon {...props} name="paper-plane-outline" />;
  return (
    <Layout style={{padding: 10, alignItems: 'center', height: '100%'}}>
      <Layout style={styles.container}>
        <TextInput
          style={styles.title}
          placeholder="Ingresa un titulo"
          {...title}
        />

        <TextInput
          multiline={true}
          style={styles.description}
          placeholder="Ingresa una descripcion"
          {...description}
        />
      </Layout>
      <Button
        style={{width: '90%'}}
        onPress={() => navigation.navigate('Wall')}
        appearance="outline"
        // accessoryRight={PublishIcon}
      >
        Publicar
      </Button>
    </Layout>
  );
};

const styles = StyleSheet.create({
  container: {paddingHorizontal: 20, paddingVertical: 10},
  title: {
    fontSize: 24,
    marginVertical: 2,
    textAlign: 'left',
  },
  description: {fontSize: 20, minHeight: 64, maxHeight: 120},
});

export default Post;
