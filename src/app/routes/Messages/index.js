import React from 'react';
import {
  TabBar,
  Tab,
  Layout,
  Text,
  TopNavigation,
  Divider,
} from '@ui-kitten/components';
import {SafeAreaView} from 'react-native';
// https://github.com/FaridSafi/react-native-gifted-chat
const Messages = () => {
  return (
    <SafeAreaView style={{flex: 1}}>
      <TopNavigation title="Messages" alignment="center" />
      <Divider />
      <Layout style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
        <Text category="h1">Messages</Text>
      </Layout>
    </SafeAreaView>
  );
};

export default Messages;
