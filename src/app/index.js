import React, {useEffect, useState} from 'react';
import {createMaterialTopTabNavigator} from '@react-navigation/material-top-tabs';
import {Icon, TabBar, Tab} from '@ui-kitten/components';
import screens from './routes';
import {StyleSheet, View} from 'react-native';
import {connect} from 'react-redux';
const {Navigator, Screen} = createMaterialTopTabNavigator();

const TopTabBarConnected = ({navigation, state, app_settings}) => {
  const {theme} = app_settings;
  return (
    <TabBar
      style={{width: '100%', backgroundColor: theme['header-color']}}
      selectedIndex={state.index}
      onSelect={index => navigation.navigate(state.routeNames[index])}>
      <Tab
        icon={props => <Icon {...props} name="home-outline" />}
        name="Home"
      />
      <Tab
        icon={props => <Icon {...props} name="people-outline" />}
        name="Community"
      />
      <Tab
        icon={props => <Icon {...props} name="star-outline" />}
        name="Following"
      />
      <Tab
        icon={props => <Icon {...props} name="email-outline" />}
        name="Messages"
      />
    </TabBar>
  );
};

const mapStateToProps = state => {
  const {app_settings} = state;
  return {
    app_settings,
  };
};

const TopTabBar = connect(mapStateToProps)(TopTabBarConnected);

const TabNavigator = props => {
  return (
    <Navigator tabBar={props => <TopTabBar {...props} />}>
      <Screen name="Home" component={screens.Home} />
      <Screen name="Community" component={screens.Community} />
      <Screen name="Following" component={screens.Following} />
      <Screen name="Messages" component={screens.Messages} />
    </Navigator>
  );
};

const TabController = props => {
  return <TabNavigator />;
};

export default TabController;
