import {sendRequest} from '../../utils';
import storage from 'services';

const getUserProfile = async (id, data) => {
  let route = `/user/${id}`;
  let request = {
    method: 'GET',
    header: {
      'Content-Type': 'application/json',
      Authentication: storage.getToken(),
    },
  };
  return await sendRequest(route, request);
};
export default {
  getUserProfile,
};
