const getData = () => {
  return JSON.parse(localStorage.getItem('data'));
};

const getToken = () => {
  let token = false;
  const data = JSON.parse(localStorage.getItem('data'));
  if (!!data) {
    token = data.token ? data.token['access_token'] : false;
  }
  return token;
};

const setStorage = data => {
  localStorage.setItem('data', JSON.stringify(data));
};

const removeToken = () => {
  let data = JSON.parse(localStorage.getItem('data'));
  if (!!data) {
    data = {data: {}};
  }
  return setStorage(data);
};

export default {
  getData,
  getToken,
  setStorage,
  removeToken,
};
