export const tabs = {
  Home: {
    icon: ['question-circle', 'question-circle-o'],
    label: 'Inicio',
  },
  Community: {
    icon: ['comments', 'comments-o'],
    label: 'Comunidad',
  },
  Following: {
    icon: ['thumb-tack', 'thumb-tack'],
    label: 'Siguiendo',
  },
  Messages: {
    icon: ['envelope', 'envelope-o'],
    label: 'Inbox',
  },
  Settings: {
    icon: ['cog', 'cog'],
    label: 'Opciones',
  },
};
export const toolbar = {
  User: {
    icon: [],
    label: '',
  },
};
