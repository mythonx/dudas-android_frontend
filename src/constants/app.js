const colors = {
  primary: '#006666',
  secondary: '#c81912',
  extra: [],
};
const identity = {
  name: 'dudasmx',
  createdBy: ['Israel Maytorena', 'Missael Hernandez', 'Max Escobedo'],
  website: 'https://www.dudasmx.com',
};

export default {
  colors,
  identity,
};
