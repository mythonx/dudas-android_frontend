import app from './app';
import {tabs} from './elements';

const baseUrl = 'http://localhost:8000';

export default {
  baseUrl,
  app,
  tabs,
};
