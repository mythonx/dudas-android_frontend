import React from 'react';
import {StyleSheet, View} from 'react-native';
import {Button, Icon, Layout, Spinner} from '@ui-kitten/components';

const LoadingIndicator = props => (
  <View style={[props.style, styles.indicator]}>
    <Spinner size="small" />
  </View>
);
// <Button style={styles.button} appearance='outline' accessoryLeft={LoadingIndicator}>
//     LOADING
// </Button>

export default LoadingIndicator;

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  button: {
    margin: 2,
  },
  indicator: {
    justifyContent: 'center',
    alignItems: 'center',
  },
});
