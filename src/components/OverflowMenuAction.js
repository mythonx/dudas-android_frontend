import React from 'react';
import {
  Icon,
  MenuItem,
  OverflowMenu,
  TopNavigationAction,
} from '@ui-kitten/components';

const OverflowMenuAction = () => {
  const [menuVisible, setMenuVisible] = React.useState(false);

  const toggleMenu = () => {
    setMenuVisible(!menuVisible);
  };
  const MenuIcon = props => <Icon {...props} name="more-vertical" />;

  const InfoIcon = props => <Icon {...props} name="info" />;

  const LogoutIcon = props => <Icon {...props} name="log-out" />;
  const renderMenuAction = () => (
    <TopNavigationAction icon={MenuIcon} onPress={toggleMenu} />
  );

  return (
    <React.Fragment>
      <OverflowMenu
        anchor={renderMenuAction}
        visible={menuVisible}
        onBackdropPress={toggleMenu}>
        <MenuItem accessoryLeft={InfoIcon} title="About" />
        <MenuItem accessoryLeft={LogoutIcon} title="Logout" />
      </OverflowMenu>
    </React.Fragment>
  );
};
export default OverflowMenuAction;
