import * as React from 'react';
import Svg, {Path} from 'react-native-svg';

/* SVGR has dropped some elements not supported by react-native-svg: title */

function Logo(props) {
  const {width, height} = props;
  return (
    <Svg
      width={props ? width : 24}
      height={props ? height : 24}
      viewBox="0 0 12.7 12.7"
      {...props}>
      <Path
        d="M2.541.004h7.616a2.533 2.533 0 012.54 2.538v7.617a2.533 2.533 0 01-2.54 2.539H2.541a2.533 2.533 0 01-2.539-2.54V2.543A2.533 2.533 0 012.542.004z"
        fill="#025"
        paintOrder="stroke markers fill"
      />
      <Path
        style={{
          lineHeight: 1.25,
          InkscapeFontSpecification: "'Croissant One'",
        }}
        d="M4.768 2.542c-.626 0-1.162.13-1.607.387-.438.257-.657.619-.657 1.085 0 .23.08.427.24.594.16.16.354.24.584.24.23 0 .428-.083.594-.25a.778.778 0 00.25-.564.819.819 0 00-.176-.532.827.827 0 00-.46-.302c.174-.077.373-.115.595-.115.46 0 .81.15 1.054.449.243.292.365.643.365 1.053 0 .41-.115.786-.344 1.127-.23.334-.616.55-1.158.647l.177 1.523h.563l.147-1.21c.626-.139 1.12-.407 1.481-.803.362-.404.543-.873.543-1.409 0-.535-.21-.987-.626-1.356-.41-.376-.932-.564-1.565-.564zm-.261 6.01a.783.783 0 00-.574.24.766.766 0 00-.23.563c0 .223.077.414.23.574.16.153.351.23.574.23.222 0 .41-.077.563-.23.16-.16.24-.351.24-.574a.75.75 0 00-.24-.563.75.75 0 00-.563-.24z"
        fontWeight={400}
        fontSize={108.733}
        fontFamily="Croissant One"
        letterSpacing={0}
        wordSpacing={0}
        fill="#fff"
      />
      <Path
        d="M.082 10.794a2.533 2.533 0 002.46 1.904h7.615a2.533 2.533 0 002.46-1.904z"
        fill="#003380"
        paintOrder="stroke markers fill"
      />
      <Path
        d="M8.862 2.542h.38v8.886h-.38z"
        fill="#f60"
        paintOrder="markers stroke fill"
      />
    </Svg>
  );
}

export default Logo;
