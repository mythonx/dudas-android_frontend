import constants from '../constants';

export const sendRequest = async (route, request) => {
  let response, data;
  try {
    response = await fetch(constants.baseUrl + route, request);
  } catch (error) {
    return {success: false};
  }
  try {
    data = await response.json();
    return data;
  } catch (error) {
    return {success: false};
  }
};

export default constants;
