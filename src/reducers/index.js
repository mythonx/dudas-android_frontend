import {combineReducers} from 'redux';
import {APP_SETTINGS} from '../actions/types';
import * as eva from '@eva-design/eva';
import {default as themeLight} from '../themes/light-theme.json';

const INITIAL_STATE = {
  byDefault: true,
  theme: {...eva.light, ...themeLight},
};

const applicationSettings = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case APP_SETTINGS:
      return {
        ...state,
        ...action.payload,
      };
    default:
      return state;
  }
};

export default combineReducers({
  app_settings: applicationSettings,
});
